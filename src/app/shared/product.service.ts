import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private products: Product[] = [
    // tslint:disable-next-line: no-use-before-declare
    new Product(1, 'First Product', 199, 4.5, 'This is a description', ['Wines', 'Alcohol']),
    // tslint:disable-next-line: no-use-before-declare
    new Product(2, 'Second Product', 249, 5.0, 'This is a description', ['Books', 'Arts', 'Crafts']),
    // tslint:disable-next-line: no-use-before-declare
    new Product(3, 'Third Product', 179, 3.5, 'This is a description', ['Vehicle', 'Motor']),
    // tslint:disable-next-line: no-use-before-declare
    new Product(4, 'Fourth Product', 200, 4.5, 'This is a description', ['Real Estate']),
    // tslint:disable-next-line: no-use-before-declare
    new Product(5, 'Fifth Product', 150, 2.5, 'This is a description', ['Jewelry']),
    // tslint:disable-next-line: no-use-before-declare
    new Product(6, 'Sixth Product', 699, 4.0, 'This is a description', ['Clothing', 'Fashion']),
  ];

  private comments: Comment[] = [
    // tslint:disable-next-line: no-use-before-declare
    new Comment(1, 1, '2020-02-02 22:22:22', 'Minhao', 3, 'Nice product!'),
    // tslint:disable-next-line: no-use-before-declare
    new Comment(2, 1, '2020-03-03 23:22:22', 'Qi', 4, 'This is a nice product!'),
    // tslint:disable-next-line: no-use-before-declare
    new Comment(3, 1, '2020-04-04 21:22:22', 'Furkan', 2, 'It is alright!'),
    // tslint:disable-next-line: no-use-before-declare
    new Comment(4, 2, '2020-05-05 20:22:22', 'Petre', 4, 'Very nice!'),
  ];


  constructor() { }

  getProducts(): Product[] {
    return this.products;
  }

  getProduct(id: number): Product {
    return this.products.find((product) => product.id == id);
  }

  getCommentsForProductId(id: number): Comment[] {
    return this.comments.filter((comment: Comment) => comment.productId == id);
  }
}



export class Product {

  constructor(
    public id: number,
    public title: string,
    public price: number,
    public rating: number,
    public desc: string,
    public categories: Array<string>
  ) {

  }
}


export class Comment {

  constructor(public id: number,
              public productId: number,
              public timestamp: string,
              public user: string,
              public rating: number,
              public content: string
  ) {

  }

}
